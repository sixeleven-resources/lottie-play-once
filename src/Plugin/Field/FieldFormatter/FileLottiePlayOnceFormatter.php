<?php

namespace Drupal\lottie_play_once\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_entity_lottie\Plugin\Field\FieldFormatter\FileLottiePlayerFormatter;

/**
 * Plugin implementation of the 'file_lottie_play_once' formatter.
 *
 * @see: https://github.com/LottieFiles/lottie-player
 *
 * @FieldFormatter(
 *   id = "file_lottie_play_once",
 *   label = @Translation("Lottie player playOnce"),
 *   description = @Translation("Like Lottie player with playOnce config."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileLottiePlayOnceFormatter extends FileLottiePlayerFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'play_once' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $play_once['play_once'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Play only once'),
      '#description'   => $this->t('Play the animation only once (when visible in viewport).'),
      '#default_value' => $this->getSetting('play_once'),
      '#states'        => [
        'unchecked' => [
          ':input[name$="[play_when_visible]"]' => [
            'checked' => FALSE,
          ],
        ],
        'visible'   => [
          ':input[name$="[play_when_visible]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $keys  = array_keys($elements);
    $index = array_search('play_when_visible', $keys);
    $pos   = FALSE === $index ? count($elements) : $index + 1;

    $elements = array_merge(array_slice($elements, 0, $pos), $play_once, array_slice($elements, $pos));

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('play_when_visible')) {
      $summary[] = $this->t('Play once: %play_once', ['%play_once' => $this->getSetting('play_once') ? $this->t('yes') : $this->t('no')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareAttributes(array $additional_attributes = []) {
    $attributes = parent::prepareAttributes();

    $attributes_array = $attributes->toArray();

    if ($attributes_array['hover'] === 0) {
      $attributes->removeAttribute('hover');
    }

    if ($this->getSetting('play_once')) {
      $attributes->setAttribute('play_once', $this->getSetting('play_once'));
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $attributes = $this->prepareAttributes();
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $attributes->setAttribute('src', $file->createFileUrl());
      $elements[$delta] = [
        '#theme' => 'file_lottie_player',
        '#attributes' => $attributes,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }
    $elements['#attached'] = [
      'library' => [
        'media_entity_lottie/lottie_player',
      ],
    ];
    if ($this->getSetting('play_once')) {
      $elements['#attached']['library'][] = 'lottie_play_once/play_once';
    }
    elseif ($this->getSetting('play_when_visible')) {
      $elements['#attached']['library'][] = 'media_entity_lottie/play_when_visible';
    }

    return $elements;
  }

}
