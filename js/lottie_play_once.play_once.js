/**
 * @file
 * Behaviours to handle playing animations when visible in viewport.
 */
(function (Drupal, $, once) {

  'use strict';

  Drupal.behaviors.lottiePlayOnce = {
    attach: (context, settings) => {
      $(once('lottie-play-once', 'lottie-player[play_once="1"]', context))
        .each((index, element) => {
          LottieInteractivity.create({
            player: element,
            mode: "scroll",
            actions: [
              {
                visibility: [0, 1.0],
                type: 'playOnce'
              },
            ],
          });
        });

    }
  }
}(Drupal, jQuery, once))
